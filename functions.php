<?php

# To disable magic quotes if they are enabled
if(function_exists('get_magic_quotes_gpc') && get_magic_quotes_gpc()){ 
    function undo_magic_quotes_gpc(&$array) { 
        foreach($array as &$value){ 
            if(is_array($value)){ 
                undo_magic_quotes_gpc($value); 
            } else{ 
                $value = stripslashes($value); 
            } 
        } 
    } 
 
    undo_magic_quotes_gpc($_POST); 
    undo_magic_quotes_gpc($_GET); 
    undo_magic_quotes_gpc($_COOKIE); 
}

# Checks user input for malicious content
function check_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function spamcheck($field){
	$field = filter_var($field, FILTER_SANITIZE_EMAIL);
	
	if(filter_var($field, FILTER_VALIDATE_EMAIL)){
		return TRUE;
	} else{
		return FALSE;
	}
}