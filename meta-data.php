<html>
	<head class='site-header'>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Web and Mobile Applications</title>
		
		<!-- favicons -->
		<link rel="shortcut icon" href="./favicon.ico">
		
		<!-- SEO -->
		<meta name="description" content="We build custom mobile and web apps. Android apps, software, responsive web design, and graphic design."/>
		<link rel="canonical" href="http://fortistech.org/" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Mobile and Web Apps Development" />
		<meta property="og:description" content="We build custom mobile and web apps. Android apps, software, responsive web design, and graphic design." />
		<meta property="og:url" content="http://www.fortistech.org/" />
		<meta property="og:site_name" content="Mobile and Web Apps Development - Fortis Tech" />
		<link rel='shortlink' href='http://fortistech.org/' />
		
		<!-- Include CSS -->
		<?php include('./php/snippet-scripts-css.php'); ?>
	</head> <!-- masthead -->