<?php

if ( $_GET ){
	if ( $_GET['page'] ){
	
		$page = $_GET['page'];
		
		switch($page){
		
			case 'about':
				$id_body = "about";
				$this_page='./php/page-about-us.php';
				$page_title = "About Us";
				$description = "Established in December 2013 - Lilongwe, Malawi";
			break;
			
			case 'contact':
				$id_body = "contact";
				$this_page='./php/page-contact-us.php';
				$page_title = "Contact Us";
				$description = "Reach Us | Email: inquiries@fortistech.org";
			break;
			
			case 'partners':
				$id_body = "partners";
				$this_page='./php/page-partners.php';
				$page_title = "Our Partners";
				$description = "Partners | Friends we have made to ensure you get only the best quality";
			break;
			
			case 'products':
				$id_body = "products";
				$this_page = "./php/page-products.php";
				$page_title = "Our Range of Products";
				$description = "Browse our range of products";
			break;
				
			case 'work':
			case 'portfolio':
				$id_body = "work";
				$this_page='./php/page-portfolio.php';
				$page_title = "Fortis Tech Limited, Malawi - Our work and portfolio";
				$description = "Our work and some of the amazing people we were fortunate to work with";
			break;
			
			default:
				$id_body = "home";
				$this_page = "./php/page-index.php";
			break;
		} # switch
		
	} else {
		$id_body = "home";
		$this_page = "./php/page-index.php";
	} # else
	
} else {
	$id_body = "home";
	$this_page = "./php/page-index.php";
}