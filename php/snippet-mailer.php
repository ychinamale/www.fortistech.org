<?php

if(!empty($_POST)){
	if((!$_POST["name"]) or (!$_POST["email"]) or (!$_POST["inquiry"])){
		header("Location: ".$action."&error=1");
		die("... redirecting");
	} else{
		
		$name = check_input($_POST["name"]);
		
		$sender_email = $_POST['email'];
		
		$mailcheck = spamcheck($sender_email);
		
		if($mailcheck==FALSE){
			header("Location: ".$action."&error=2");
			die("... redirecting");
		} else{
		
			if($_POST['organization']){
				$organization = check_input($_POST["organization"]);
			}
			
			if(!empty($_POST['message'])){
				$message = "Hi,I am ".$name."<br /><br />".check_input($_POST['message']);
			} else{
				$message = "Hi,I am ".$name."<br /><br />Please get back to me. Thanks.";
			}
			
			$num = count($_POST['inquiry']);
			$inquiry = $_POST['inquiry'];
			
			$title ="";
			for($i=0;$i<$num;$i++){
				$title = $title.ucfirst($inquiry[$i]);
				if(($num>1)&&($i==$num-2)){
					$title = $title." and ";
				} else if($i!=$num-1){
					$title = $title.", ";
				}
			}
			
			$message = wordwrap($message, 70);
			
			$title = "Inquiry - ".$title;
			
			mail("inquiries@fortistech.org",$title,$message,"From: $sender_email/n");
			header("Location: ".$action."&send=success");
			die("... redirecting");
		}
		
	}
}