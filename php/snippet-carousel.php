<section id='container-carousel'>
<div class=''>
	<div id='front-carousel' class='carousel slide hidden-xs' data-ride='carousel' data-interval='4000'>
		<ol class='carousel-indicators'>
			<li data-target='#front-carousel' data-slide-to='0'></li>
			<li data-target='#front-carousel' data-slide-to='1' ></li>
			<li data-target='#front-carousel' data-slide-to='2' ></li>
		</ol>
		
		<section class='carousel-inner'>
			<div class='item active'><img src='./images/chemistry.png' alt='Mobile and Web Development'></div>
			<div class='item'><img src='./images/circuitblue.png' alt='Piano'></div>
			<div class='item'><img src='./images/balls.png' alt='Stairs to heaven'></div>
		</section><!-- #carousel-inner -->
		
		<a href='#front-carousel' class='left carousel-control' data-slide='prev' title='Previous'><span class='glyphicon glyphicon-chevron-left'></span></a>
		<a href='#front-carousel' class='right carousel-control' data-slide='next' title='Next'><span class='glyphicon glyphicon-chevron-right'></span></a>
	</div><!-- front-carousel -->
</div><!-- front-carousel -->
</section>