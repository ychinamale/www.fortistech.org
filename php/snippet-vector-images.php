<section class="">
	<div class="modalphotos photogrid clearfix">
		<img src="./images/test-a-tn.jpg">
	</div> <!-- .modalphotos -->
	
	<div id="modal" class="modal fade">
		<section class="modal-dialog modal-lg">
			<div class="modal-content"  align="center">
				<section class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Vector Image</h4>
				</section>
				<section class="modal-body">
					<img id="modalimage" src="" alt="Vector Image"> 
				</section> <!-- #modalimage -->
				<section class="modal-footer">
					<button type="button" class="btn btn-default">1280p x 800p</button>
					<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
					<a href="./" type="button" class="btn btn-primary" title="Purchase Product">Purchase</a>
				</section>
			</div>
		</section>
	
		
	</div> <!-- modal -->
</section> <!-- .media -->