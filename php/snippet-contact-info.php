<br />
<section id='contact-info' class='font-b'>
	<address>
		<strong>POSTAL ADDRESS</strong><br /><br />
		Fortis Tech, LTD.<br />
		P.O. BOX 845<br />
		LILONGWE<br />
		MALAWI<br />
	</address>
	<address>
		<span class="small glyphicon glyphicon-envelope"></span>
		<a title='Email Address'>inquiries@fortistech.org</a>
	</address>
</section> <!-- #contact-info -->