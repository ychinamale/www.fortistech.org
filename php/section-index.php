<section class='container'>
	<div class='content row'>
		
		<section id='main' class='col col-lg-8'>
			<h2>Services</h2>
			<p>This is where you will find the information for all the things.Our MAJOR Focus is creating the ultimate Mobile User Experience. We specialize in mobile apps, but also design responsive websites, build web apps, not to mention branding and illustration. We have a proven process that begins with strategy and concept validation to project planning, team and asset provisioning, and on down to design and development. Visit our Services page to learn more, or check out our Work.</p>
		</section> <!-- main>
		
	</div> <!-- #row -->

</section> <!-- #container -->