<div>
	<nav class='navbar-inverse navbar-fixed-top'>
		<section class='container'>
			<a id='logo' href='./' class='navbar-brand pull-left' title='Fortis Tech Limited, Malawi'>
				<b>[ Fortis Tech ]</b>
			</a>
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#top-navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button> <!-- #navbar-toggle -->
			</div> <!-- navbar-header -->
			
			<div id='top-navbar' class='collapse navbar-collapse navbar-inverse font-b' role='navigation'>		
				<ul class='nav navbar-nav'>
					<li><a href="./" title="Fortis Tech Limited, Malawi">Home</a></li>
					<li><a href="./#services" title="Services we offer" class="hidden-xs">Services</a></li>
					<li><a href="./?page=contact" title="Our contact information">Contact Us</a></li>
					<li><a href="./?page=products" title="Our range of products">Products</a></li>
				</ul> <!-- .nav navbar-nav -->		
			</div> <!-- .navbar-collapse navbar-inverse -->
		</section> <!-- .container -->
	</nav> <!-- .navbar-inverse -->
</div> <!-- #top-navbar -->