<section class='panel-group' id='accordion'>
	<div class='panel'>
		<section class='panel-heading btn-primary' data-toggle='collapse' data-parent='#accordion'  href='#info-complab'>
			<h4 class='panel-title'>
				<a href='#info-complab' title='Complab Kenya panel link'>
					<span class="glyphicon glyphicon-th-list"></span> COMPLAB, KENYA
				</a>
			</h4>
		</section>
		<section id='info-complab' class='panel-collapse collapse out'>
			<div class='panel-body'>
				<section class='media'>
					<div class='pull-left hidden-xs'>
						<img class='media-object img-thumbnail' src='./images/thumb-md-complab.jpg' alt='Complab Kenya Image'>
					</div>
					<div class='media-content font-b'><br/>
						<a href='http://www.complab.co.ke' title='Complab Website Link'>CompLab</a> is a Nairobi-based IT company whose mission is to facilitate the transformation of service delivery in East Africa. They believe this can be achieved through adaptation of ICT systems.<br/><br/>
						CompLab are specialists at 3D visualization, graphic design, product design and web development.
					</div>
				</section><!-- media -->
			</div>
		</section>
	</div> <!-- .panel -->
	<div class='panel'>
		<section class='panel-heading btn-success' data-toggle='collapse' data-parent='#accordion'  href='#info-bboyu'>
			<h4 class='panel-title'>
				<a href='#info-bboyu' alt='Tiny Victory Studio panel link'>
					<span class="glyphicon glyphicon-th-list"></span> TINY VICTORY STUDIOS, MALAWI
				</a>
			</h4>
		</section> <!-- panel-heading -->
		<section id='info-bboyu' class='panel-collapse collapse out'>
			<div class='panel-body'>
				<section class='media'>
					<div class='pull-left hidden-xs'>
						<img class='media-object img-thumbnail' src='./images/thumb-md-tiny-victory.png' alt='Tiny Victory Studios Image'>
					</div> <!-- pull-left-->
					<div class='media-content font-b'><br/>
						Operating from the nation's capital - Lilongwe, we bring you Tiny Victory Studios - the team that brought you <b>The Spectacular Blue Boy</b> (the android game). They are avid designers, developers and all round innovators.<br/><br/>
						The studio focuses on software and game development for PC, Android and iOS using Unity 3D. But that's not all. Art, illustrations and graphic design are also part of their repertoire.
					</div> <!-- -->
				</section> <!-- .media -->
			</div> <!-- panel-body -->
		</section> <!-- #info-bboyu -->
	</div> <!-- .panel -->
</section> <!-- #accordion -->