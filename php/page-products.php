<section class="container" id="page-products">
	<br/>
	<div class="tabbable tabs">
		<ul class="nav nav-pills">
			<li class="active"><a href="#vector-images" data-toggle="tab" title="Stock Images">Stock Images</a> </li>
			<li> <a href="#website-templates" data-toggle="tab" title="Website Templates">Website Templates</a> </li>
		</ul> <!-- nav-pills -->
	</div> <!-- .tabbable .tabs -->
	
	<div class="tab-content">
		<br/>
		<div class="tab-pane active" id="vector-images">
			<?php include ("./php/snippet-vector-images.php"); ?>
		</div> <!-- #vector-images -->
		
		<div class="tab-pane" id="website-templates">
			<?php include ("./php/snippet-website-templates.php"); ?>
		</div> <!-- website-templates -->
	</div> <!-- .tab-content -->
	<br/>
</section> <!-- #page-products -->

<?php
	include ("./php/snippet-google-analytics");