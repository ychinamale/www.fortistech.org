<div id='footer' class='container-fluid'>
		
		<section class='container'>
			<div class='col col-lg-9'>
				<?php include('./php/snippet-contact-info.php'); ?>
			</div> <!-- .col-lg-8 -->
			
			<div class='col col-lg-3'>
				<?php include('./php/snippet-social-links.php'); ?>
			</div> <!-- .col-lg-8 -->
		</section> <!-- .content row -->
		
		<?php 
			include('./php/snippet-breadcrumbs.php');
		?>
</div>