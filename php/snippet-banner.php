<section id="banner" style="background-image: url('images/orange_opt.png');" class='hidden-xs' title="Background image - Fortis Tech Limited">
    <br>
    <div class='front-banner'>
		<h1 class='heading'><b>Fortis Tech</b><br/></h1>
		<p class='banner-text'>We make great applications.<br/>Web and mobile platforms are our speciality.</p>
		<br />
		<a class='btn btn-primary btn-lg' href='./view.php?page=contact' title="Request a project">I WANT ONE</a>
    </div>
</section>