<section id='container-services' class='clearfix'>
	<a id='services' title='What Fortis Tech does'></a>
	<div class='services container'>
		<br />
		<section class='panel visible-xs'>
			<div class='panel-heading btn-warning'>
				<h2 class='panel-title' align='center'>
					What Fortis Tech does
				</h2> <!-- .panel-title -->
			</div> <!-- .panel-heading -->
		</section><!-- panel -->
		<div class='tabbable tabs'>	
			<ul class='nav nav-tabs nav-justified font-b'>
				<li class="active"><a href="#android" data-toggle="tab" title="Toggle Mobile Apps">Mobile Apps</a></li>
				<li><a href="#responsive" data-toggle="tab" title="Toggle Web Design">Web Design</a></li>
				<li><a href="#graphic" data-toggle="tab" title="Toggle Branding">Branding</a></li>
			</ul> <!-- .nav .nav-tabs -->
		</div> <!-- .tabbable .tabs -->

		<section class='tab-content'> <!-- #getting tab content using php includes -->
			<br />
			<div class='tab-pane active' id='android'>
				<?php include('article-android-development.php'); ?>
			</div><!-- #android .tab-pane -->
			
			<div class='tab-pane' id='graphic'>
				<?php include('article-graphic-design.php'); ?>
			</div><!-- #graphic .tab-pane -->
			
			<div class='tab-pane' id='responsive'>
				<?php include('article-responsive-design.php'); ?>
			</div><!-- #responsive .tab-pane -->
		</section> <!-- .tab-content -->
		<br />
	</div> <!-- .container -->
</section>