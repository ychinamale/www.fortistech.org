<div  class='container'>
<br />
	<section class='jumbotron container-fluid'>
		<a class='btn btn-group btn-warning btn-lg' title='Strategic Partners'><b>STRATEGIC PARTNERS</b></a><br />
		<p class='col-lg-1'></p>
		<p class='col-lg-11 small'><br />
		We are dedicated to your success. We have teamed up with top organizations in the industry to ensure your solutions are efficient in performance, secure in implementation and aesthetic in design.
		</p>
		<!-- <p class='col-lg-1'></p> -->
	</section> <!-- .jumbotron -->
	<section id='partners'>
		<?php include('./php/snippet-partner-list.php'); ?>
	</section>
</div> <!-- .container-fluid, for max-width -->
<?php
	include ("./php/snippet-google-analytics");