<!-- About Section - Graphic Design -->
<div class='media'>
	<a class='pull-left hidden-xs' href='#' title='Graphic design'>
		<img class='media-object img-thumbnail' src='./images/thumb-sm-abstract.png' alt='Graphic design'>
	</a>
	
	<div class='media-body'>
		<h4 class='media-heading hidden-xs'></h4>
		<p class='media-content font-b'>
		We are passionate about delivering innovative products and seeing our clients succeed.<br/><br/>
		Our team of design artists will help bring your ideas to life. Don't settle for just another logo. We will help you create a work of art that tells a story. A story that demands attention, engages your audience and leaves a lasting impression. <br/><br/>
		If this interests you, <a href='./view.php?page=contact'>click here</a>.
		</p>
	</div>
</div> <!-- .media -->