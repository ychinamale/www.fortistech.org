<!-- About Section - Android Development -->
<div class='media'>
	<a class='pull-left hidden-xs' href='#' title="Mobile app development">
		<img class='media-object img-thumbnail' src='./images/thumb-sm-android.png' alt='Mobile app development'>
	</a>
	
	<div class='media-body'>
		<h4 class='media-heading hidden-xs'></h4>
		<p class='media-content font-b'>
		Enhance your mobile experience with custom-made android applications. We build user friendly, highly efficient apps that wont strain your device. We cater for diverse needs including business, gaming, social networks and enterprise solutions. We don't just convert your instructions to code. We use your guidance to create an engaging, awe-inspiring mobile experience. We create applications you'll be proud to say you founded/are a part of.<br/><br/>
		Our development team strongly believes agile development methodologies are most suitable to ensure your product remains relevant to the final user. Which is why we walk with you every step of the way and provide a viable product at each milestone in the development process.<br/><br/>
		If you'd like us to take this journey with you, <a href='./view.php?page=contact'>start here</a>.
		</p>
	</div>
</div> <!-- .media -->