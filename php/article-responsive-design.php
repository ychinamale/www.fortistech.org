<!-- About Section - Responsive Design -->
<div class='media'>
	<a class='pull-left hidden-xs' href='#' title='Responsive Design'>
		<img class='media-object img-thumbnail' src='./images/thumb-sm-responsive-design.png' alt="Responsive design">
	</a>
	
	<div class='media-body'>
		<h4 class='media-heading hidden-xs'></h4>
		<p class='media-content font-b'>
		We deliver functional, rich, high-standard web applications that make our clients stand out. Using a combination of HTML5, CSS3 and JQuery we deliver web experiences for numerous categories of interest - artist portfolios, mapping services, online education platforms, social media, video streaming. Responsive design allows your application to respond to the users' browsing environment based on screen size, platform and orientation.<br/><br/>
		Using a combination of JQuery and CSS, your application will automatically switch presentation to accommodate for the resolution, media capabilities and screen size of the device you are using. This means that your app will be fully functional across different platforms.<br/><br/>
		If this interests you, <a href='./view.php?page=contact'>click here</a>.
		</p>
	</div>
</div> <!-- .media -->