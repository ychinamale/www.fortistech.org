<br />
<section id='services' class='container col'>

	<div class='tabbable tabs'>	
		<ul class='nav nav-tabs nav-justified'>
			<li class="active"><a href="#android" data-toggle="tab">ANDROID APPS</a></li>
			<li><a href="#graphic" data-toggle="tab">GRAPHIC DESIGN</a></li>
			<li><a href="#responsive" data-toggle="tab">RESPONSIVE DESIGN</a></li>
		</ul> <!-- .nav .nav-tabs -->
	</div> <!-- .tabbable .tabs -->

	<section class='tab-content'> <!-- #getting tab content using php includes -->
		<br />
		<div class='tab-pane active' id='android'>
			<?php include('article-android-development.php'); ?>
		</div><!-- #android .tab-pane -->
		
		<div class='tab-pane' id='graphic'>
			<?php include('article-graphic-design.php'); ?>
		</div><!-- #graphic .tab-pane -->
		
		<div class='tab-pane' id='responsive'>
			<?php include('article-responsive-design.php'); ?>
		</div><!-- #responsive .tab-pane -->
		
	</section> <!-- .tab-content -->
	
</section> <!-- .container -->
<br />