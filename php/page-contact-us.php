<?php

	// $action helps to trace this page as mail is being processed
	$action = htmlspecialchars($_SERVER['PHP_SELF'])."?page=contact";

	if($_POST){  /* If form input is submitted, use mailer to handle input */
		include('./php/snippet-mailer.php');
	}
?>

<section id='contact-page'>
	<br />
	<div class='container medium-font'>
		<section class='content row'>
			<div class='col-lg-2'></div>
			
			<?php if(isset($_GET['send'])){ 
			?>
				<div class='col-lg-8' align='center'>
					<h3><span class='label label-success'>Thank you. We will get back to you soon.</span></h3>
				</div>
			<?php } else if(isset($_GET['error'])){ ?>
				<div class='col-lg-8' align='center'>
					<h3><span class='label label-warning'>Oops. We need your name, email and at least one option ticked.</span></h3>
				</div>
			<?php } else { ?>
				<div class='col-lg-8' align='center'>
					<h3><span class='label label-primary'>We would like to hear from you.</span></h3>
				</div>
			<?php } ?>
			
			<div class='col-lg-2'></div>
		</section><br />
		<form class='form-horizontal' role='form' action='<?php echo $action; ?>' method='POST'>
			<div class='form-group'>
				<label for='name' class='control-label col-lg-2'></label>
				<div class='col-lg-8'>
					<input name='name' id='name' type='text' class='form-control' placeholder='Full name'>
				</div>
				<div class='col-lg-2'></div> <!-- name right padding -->
			</div><!-- .form-group -->
			
			<div class='form-group'>
				<label for='email' class='control-label col-lg-2'></label>
				<div class='col-lg-8'>
					<input name='email' id='email' type='email' class='form-control' placeholder='Email address'>
				</div>
				<div class='col-lg-2'></div> <!-- email right padding -->
			</div><!-- .form-group -->
			
			<div class='form-group'>
				<label for='organization' class='control-label col-lg-2'></label>
				<div class='col-lg-8'>
					<input name='organization' id='organization' type='text' class='form-control' placeholder='Organization'>
				</div>
				<div class='col-lg-2'></div> <!-- organization right padding -->
			</div><!-- .form-group -->
			
			<div class='form-group'>
				<div class='control-label col-lg-2'></div>
				
				<section class='controls col-lg-8'>
					<label>How may we help?</label>
					<p class='checkbox'>
						<input name='inquiry[]' type='checkbox' value='advertisement' />I would like to <b>Advertise</b> on your site
					</p>
					<p class='checkbox'>
						<input name='inquiry[]' type='checkbox' value='android development' />It's about <b>Android Development</b>
					</p>
					<p class='checkbox'>
						<input name='inquiry[]' type='checkbox' value='graphic design' />It's about <b>Graphic Design</b>
					</p>
					<p class='checkbox'>
						<input name='inquiry[]' type='checkbox' value='website development' />It's about <b>Responsive Web Design</b>
					</p>
					<div class='col-lg-2'></div> <!-- checkbox right padding -->
				</section< <!-- .controls col-lg-10 -->
				
			</div><!-- .form-group -->
			
			<div class='form-group'>
				<label class='control-label col-lg-2' for='message'></label>
				<div class='col-lg-8'>
					<textarea name='message' id='message' type='text' class='form-control' rows='5' placeholder='More specifically...'></textarea>
				</div><!-- col-lg-8 -->
				<div class='col-lg-2'></div> <!-- message right padding -->
			</div><!-- .form-group -->
			
			<div class='col-lg-2'></div> <!-- submit button left padding --> 
			<div class='col-lg-8'>
				<input type='hidden' name='email-form' value='confirmed' />
				<button type='submit' class='btn btn-primary'>Submit</button>
			</div> <!-- .col-lg-8 -->
			<div class='col-lg-2'></div> <!-- submit button right padding -->
			
		</form> <!-- horizontal form -->
	</div> <!-- .container -->
</section> <!-- #contact-page -->
<br />
<?php
	include ("./php/snippet-google-analytics");