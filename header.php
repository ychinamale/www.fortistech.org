<?php
	if ( ! isset($page_title) ) {
		$title = "Web and Mobile Applications";
	} else {
		$title = $page_title;
	}
?>
<html>
	<head class='site-header'>
	
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>
			<?php
				echo $title;
			?>
		</title>
		
		<!-- favicons -->
		<link rel="shortcut icon" href="./favicon.ico">
		
		<!-- SEO -->
		<meta name="description" content="<?php echo $description; ?>"/>
		<meta name="keywords" content="Fortis, Tech, Limited, Malawi, Android, Mobile, Graphics, Website, Design, Development">
		<link rel="canonical" href="http://www.fortistech.org" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Fortis Tech Limited, Malawi" />
		<meta property="og:description" content="We create applications for mobile platforms. Work with us for apps that stand out and do exactly what they are meant to - great things. Custom android apps, website design and development." />
		<meta property="og:url" content="http://www.fortistech.org" />
		<meta property="og:site_name" content="Fortis Tech Limited, Malawi" />
		<link rel='shortlink' href='http://fortistech.org' />
		
		<!-- Include CSS -->
		<?php include ( "./php/snippet-scripts-css.php" ); ?>
		
	</head> <!-- masthead -->
	
	<body id="<?php echo $id_body; ?>">
		<a id='top' title="Top anchor"></a><!-- to jump 'back to top' -->

		<?php include( "functions.php" ); ?>
		<div id='header'>
				<header class='clearfix'>
						<?php include ( "./php/snippet-branding.php"); ?>
						<?php include ( "./php/snippet-navbar.php" ); ?>
						<?php include ( "./php/snippet-access.php" ); ?>
				</header> <!-- .clearfix -->
		</div> <!-- #header -->