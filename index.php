<?php
	
	$page_title = "Fortis Tech Limited, Malawi";
	$description = "We create applications for mobile platforms. Work with us for apps that stand out and do exactly what they are meant to - great things. Custom android apps, website design and development.";
	
	include ( "./view.php" );
	include ( "./header.php" );
	include ( $this_page );
	include ( "./footer.php" );