$( document ).ready( function( ){

	//highlight the current navigation
	$('#home a:contains("Home")').parent( ).addClass('active');
	$('#contact a:contains("Contact Us")').parent( ).addClass('active');
	$('#products a:contains("Products")').parent ( ).addClass('active');

	// Change the colour of the active about-us tab	
	$(".nav-tabs > li").click(function( ){
		$(".nav-tabs > li").children( ).css("background-color","#FFFFFF").css("color","555555");
		$(this).children( ).css("background-color","#0090F0").css("color","#FFFFFF");
	});
	
	//show modals
	
	$(".modalphotos img").on("click", function() {
		$("#modal").modal({
			show:true,
			})
			
			var mysrc = this.src.substr(0, this.src.length - 7) + ".jpg";
			$("#modalimage").attr("src",mysrc);
			$("#modalimage").on("click", function( ) {
				$("#modal").modal("hide");
			});
	});
});